# GitLab GUI Trailing Spaces affect

## Triple whitespace present

Random text. Random text. Random text. Random text.   
Random text. Random text. Random text. Random text.   
Random text. Random text. Random text. Random text.   

## Double whitespace present

Random text. Random text. Random text. Random text.<br>
Random text. Random text. Random text. Random text.
Random text. Random text. Random text. Random text.<br>

## Single whitespace present

Random text. Random text. Random text. Random text. 
Random text. Random text. Random text. Random text. 
Random text. Random text. Random text. Random text. 

## No whitespaces

Random text. Random text. Random text. Random text.
Random text. Random text. Random text. Random text.
Random text. Random text. Random text. Random text.
